#!/usr/bin/env bash

# Download the url list
gallery-dl -j "https://www.artstation.com/pao" |

# Discard the metadata we don't need
jq --tab \
	'{
		"paulchadeisson": [
			.[] | if .[0] == 3 and .[2].asset.asset_type == "image" then
				{
					"name": "[\(.[2].title)] \(.[2].asset.title)",
					"image": .[1] | split("?")[0]
				}
			else
				empty
			end
		]
	}' > paulchadeissonwallpapers.json
